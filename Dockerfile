FROM registry.hub.docker.com/library/python:3.13-alpine

ENV PORT=5000
WORKDIR /code

COPY pyproject.toml poetry.lock README.md /code/
COPY ./burger_maker /code/burger_maker

# hadolint ignore=DL3018,DL3019
RUN apk upgrade --no-cache \
    && apk add wget \
    && pip install --no-cache-dir .

EXPOSE ${PORT}
# hadolint ignore=DL3025
CMD uvicorn burger_maker.main:app --host=0.0.0.0 --port=${PORT}

HEALTHCHECK --interval=30s --timeout=5s --start-period=5s --retries=3 \
    CMD [ "wget", "--quiet", "--tries=1", "--spider", "http://localhost:${PORT}/health" ]