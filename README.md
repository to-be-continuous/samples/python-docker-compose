# Python + Docker + Docker Compose project sample

This project sample shows the usage of _to be continuous_ templates:

* Python
* Spectral
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))
* Docker
* Docker Compose
* Postman

The project deploys a basic serverless API developped in Python (3.9) on AWS Lambda, and implements automated accetance tests with Postman.

## Python template features

This project uses the following features from the GitLab CI Python template:

* Uses the `pyproject.toml` build specs file with [Poetry](https://python-poetry.org/) as build backend,
* Enables the [pytest](https://docs.pytest.org/) unit test framework by declaring the `$PYTEST_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables [pylint](https://pylint.pycqa.org/) by declaring the `$PYLINT_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables the [Bandit](https://pypi.org/project/bandit/) SAST analysis job by declaring the `$BANDIT_ENABLED` and skips the `B311`
  test by overriding `$BANDIT_ARGS` in the `.gitlab-ci.yml` variables.

The Python template also enforces:

* [test report integration](https://docs.gitlab.com/ci/testing/unit_test_reports/),
* and [code coverage integration](https://docs.gitlab.com/user/project/pipelines/settings/#test-coverage-report-badge).

## Spectral template features

This project uses the [Spectral](https://docs.stoplight.io/docs/spectral) template to continuously Lint the [OpenAPI specification](./burger_maker/burger_maker.openapi.yml).

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report (from `pytest`),
    * unit test reports (from `pytest`).

## Docker template features

This project builds and _pushes_ a Docker image embedding the built Python application.
For this, it is using the GitLab container registry.

You simply have to make sure the remote Docker host has (network) access to the GitLab registry (true in our case through the internet and the registry is public).

## Docker Compose template features

This project uses the following features from the Docker Compose template:

* Enables review environments by declaring the `$DCMP_REVIEW_DOCKER_HOST` variable,
* Enables staging environment by declaring the `$DCMP_STAGING_DOCKER_HOST` variable,
* Enables production environment by declaring the `$DCMP_PROD_DOCKER_HOST` variable,
* Defines `$DCMP_ENVIRONMENT_URL` with late variable expansion (`%{environment_name}`) (TODO),
* Defines the `$DCMP_SSH_KNOWN_HOSTS` variable to allow SSH Docker host,
* Defines :lock: `$DCMP_SSH_PRIVATE_KEY` as a secret project variable.

The Docker Compose template also implements [environments integration](https://docs.gitlab.com/ci/environments/) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related feature branch is deleted).

### implementation details

In order to perform Docker Compose deployments, this project implements:

* `compose.yml`: common Compose file that instantiates all required Docker Compose objects (services, volumes, networks...),
* `compose-dev.override.yml`: an override of the common Compose file for developers only (enables local Docker build).

The Compose file makes use of variables dynamically evaluated and exposed by the Docker Compose template:

* `${environment_name}`: the application target name to use in this environment (ex: `python-docker-compose-review-fix-bug-12`)
* `${docker_image_digest}`: the docker image (with digest) that was just built in the upstream pipeline and that is being deployed
* `${stage}`: the CI job stage (equals `$CI_JOB_STAGE`)
* `${hostname}`: the environment hostname, extracted from `$CI_ENVIRONMENT_URL` (declared as [`environment:url`](https://docs.gitlab.com/ci/yaml/#environmenturl) in the `.gitlab-ci.yml` file)

## Postman

This project also implements Postman acceptance tests, simply storing test collections in the default [postman/](./postman) directory.

The upstream deployed environment base url is simply referenced in the Postman tests by using the [{{base_url}} variable](https://learning.postman.com/docs/sending-requests/variables/) evaluated by the template.

## Bruno

This project also implements Bruno acceptance tests, simply storing test collections in the default [bruno/](./bruno) directory.

The upstream deployed environment base url is simply referenced in the Bruno tests by using the [{{base_url}} variable](https://docs.usebruno.com/scripting/vars) evaluated by the template.
